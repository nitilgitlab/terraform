#!/bin/bash

LOGFILE=realmsetup_`date +%H:%M:%S`.log

echo "`date +%H:%M:%S` : Starting work" >> $LOGFILE

su - openam

cd /app/openam/setup

./add-agent.sh web-demo2-css.properties 

echo "`date +%H:%M:%S` : ./add-agent.sh executed" >> $LOGFILE

service openam stop

echo "`date +%H:%M:%S` : openam stopped" >> $LOGFILE

sleep 10

echo "`date +%H:%M:%S` : openam start" >> $LOGFILE

service openam start