variable "aws_region" {
  description = "Region for the VPC"
  default = "ap-southeast-1"
}

variable "vpc_name" {
  description = "VPC NAME"
  default="demo7-vpc"
}

variable "env_name" {
  description = "ENV NAME"
  default="demo7"
}

variable "css_ear_version" {
  description = "CSS EAR VERSION"
  default="8.7.21903-SNAPSHOT"
}

variable "alfr-aws_public_url" {
  description = "Alfresco Public Accessible DNS"
  default="alfr-aws.web-apps.co.nz"
}

variable "demo-css_public_url" {
  description = "DEMO CSS Public Accessible DNS"
  default="demo4-aws.web-apps.co.nz"
}

variable "auth-aws_public_url" {
  description = "OpenAM Authentication Public Accessible DNS"
  default="auth-aws.web-apps.co.nz"
}


variable "vpc_cidr" {
  description = "CIDR for the VPC"
  default = "10.70.0.0/16"
}

variable "internet_cidr" {
  description = "CIDR for the VPC"
  default = "0.0.0.0/0"
}


variable "public_subnet_cidr" {
  description = "CIDR for the public subnet"
  default = "10.70.0.1/29"
}

variable "private_subnet_1_cidr" {
  description = "CIDR for the private subnet"
  default = "10.70.0.8/29"
}


variable "openam-ami" {
  description = "Centos OpenAM AMI"
  default = "ami-08d3fe512634c8b51"
}

variable "alfresco-ami" {
  description = "Centos Alfresco AMI"
  default = "ami-0513391ad7e2fde88"
}

variable "sqlserver-ami" {
  description = "SQL Server 2014 AMI"
  default = "ami-0558b49ce978726ef"
}

variable "haproxy-ami" {
  description = "SQL Server 2014 AMI"
  default = "ami-039dbe36b5b31b3d7"
}

variable "windowsbastion-ami" {
  description = "Windows Bastion Host"
  default = "ami-0ec7f2cc26d3d1ee1"
}

variable "bastionhost-ami" {
  description = "Linux Bastion Host"
  default = "ami-0f601d43dbe58ea94"
}

variable "css-ami" {
  description = "CSS Server AMI"
  default = "ami-038e3f0ce7c2ac18d"
}

variable "key_path" {
  description = "SSH Public Key path"
  default = "id_rsa_terraform_lab.pub"
}

variable "ssh_password" {
  description = "SSH Password"
  default = "Rev0lt1x01"
}
