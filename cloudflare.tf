provider "cloudflare" {
  email = "nitil.chaudhary@chelmer.co.nz"
  token = "ffff333d8df89a838c264b2938fe4c96141c9"
}

variable "domain" {
  default = "web-apps.co.nz"
}

resource "cloudflare_record" "auth-aws" {
  domain  = "web-apps.co.nz"
  name    = "${var.auth-aws_public_url}"
  value   = "${aws_instance.haproxy.public_ip}"
  type    = "A"
  proxied = true
}

resource "cloudflare_record" "auth-aws-new" {
  domain  = "web-apps.co.nz"
  name    = "${var.env_name}-auth-aws.web-apps.co.nz"
  value   = "${aws_instance.haproxy.public_ip}"
  type    = "A"
  proxied = true
}



#resource "cloudflare_record" "auth-aws1" {
#  domain  = "web-apps.co.nz"
#  name    = "auth-aws1.web-apps.co.nz"
#  value   = "${aws_instance.haproxy.public_ip}"
#  type    = "A"
#  proxied = true
#}

resource "cloudflare_record" "alfr-aws" {
  domain  = "web-apps.co.nz"
  name    = "${var.alfr-aws_public_url}"
  value   = "${aws_instance.haproxy.public_ip}"
  type    = "A"
  proxied = true
}

resource "cloudflare_record" "demo-aws" {
  domain  = "web-apps.co.nz"
  name    = "${var.env_name}-aws.web-apps.co.nz"  
  value   = "${aws_instance.haproxy.public_ip}"
  type    = "A"
  proxied = true
}