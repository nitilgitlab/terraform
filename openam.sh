#!/bin/bash

LOGFILE=startopenam_`date +%H:%M:%S`.log

echo "`date +%H:%M:%S` : Starting work" >> $LOGFILE

yum install sshpass -y

echo "`date +%H:%M:%S` : sshpass installed" >> $LOGFILE

su - openam

service openam stop

echo "`date +%H:%M:%S` : openam stopped" >> $LOGFILE

sleep 10

echo "`date +%H:%M:%S` : openam start" >> $LOGFILE

service openam start