#!/bin/bash

LOGFILE=/opt/scripts/install_css_ear_`date +%H:%M:%S`.log

ear_version=$1

echo 'ear_version $ear_version' > $LOGFILE

cd /home/bamboo/css-ear-install

sudo chown -R bamboo:bamboo *

cd /home/bamboo/css-ear-install

echo 'sleep for 90 seconds' > $LOGFILE

sleep 90s

echo 'resumed from sleep, about to execute a install command' > $LOGFILE

nohup bash bamboo/bamboo-scripts/css-ear/install-css-ear.sh $ear_version css-*.ear &