#! /bin/bash

sudo yum install sshpass -y

export SSHPASS=${ssh_pass}

sshpass -e scp /tmp/config.json root@${alfresco_private_ip}:/etc/hosts

sshpass -e scp /tmp/config.json root@${css_private_ip}:/etc/hosts
sshpass -e scp /tmp/startcss.sh root@${css_private_ip}:/opt/scripts/
sshpass -e scp /tmp/install_css_ear.sh root@${css_private_ip}:/opt/scripts/

sshpass -e scp /tmp/web.properties root@${openam_private_ip}:/app/openam/setup/
sshpass -e scp /tmp/web-demo2-css.properties root@${openam_private_ip}:/app/openam/setup/
sshpass -e scp /tmp/add-agent.sh root@${openam_private_ip}:/app/openam/setup/
sshpass -e scp /tmp/openam.sh root@${openam_private_ip}:/opt/scripts/

sshpass -e scp /tmp/realmsetup.sh root@${openam_private_ip}:/opt/scripts/

sshpass -e scp /tmp/starthaproxy.sh root@${haproxy_private_ip}:/opt/scripts/

#sshpass -e scp -r /home/centos/css-ear-install root@${css_private_ip}:/home/bamboo/