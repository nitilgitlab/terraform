import json

import requests
import boto3
import paramiko
import botocore
import time
import sys
import logging



logging.basicConfig(filename='startapplications.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)

logging.info('This is an info message')

logging.info ("the script has the name %s" % (sys.argv[0]))

css_version = sys.argv[1]

logging.info ("the script has the css_version %s" % (sys.argv[1]))


try:
    
    ip = requests.get("http://checkip.amazonaws.com/")
    ec2 = boto3.resource('ec2',"${aws_region}")
        
    instance_ids = ["${openam_instance_id}","${css_instance_id}","${haproxy_instance_id}","${openam_instance_id}","${css_instance_id}"] #,${alfr_instance_id}]
        
    scripts = ["openam.sh","startcss.sh","starthaproxy.sh","realmsetup.sh","install_css_ear.sh"]

   


    counter=0
        
    for instance_id in instance_ids:
        instance = ec2.Instance(instance_id)

        logging.info("counter is %s" %(counter))
        logging.info("instance.state is %s" %(instance.state))
        
        if(instance.state['Name'] == "running"):
                # Start the instance
            logging.info("Instances are runnings")

            # logging.info few details of the instance
            logging.info("Instance id - %s" %(instance.id))
            logging.info("Instance public IP - %s" %(instance.public_ip_address))
            logging.info("Instance private IP - %s" %(instance.private_ip_address))
            logging.info("Public dns name - %s" %(instance.public_dns_name))
            logging.info("----------------------------------------------------")
            
            if(counter>-1):
                ssh = paramiko.SSHClient()
                ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
                    
                logging.info("Try connecting ssh %s" %(instance.private_ip_address))
                
                ssh.connect(
                    instance.private_ip_address, username='root', password='Rev0lt1x01'
                    )
          
                logging.info("SSH connection is established")
                logging.info("Executing scipt")

                if(counter == 4):
                   #time.sleep(90)
                   script= 'chmod +x /opt/scripts/'+scripts[counter] + ' && nohup /opt/scripts/'+scripts[counter]+' '+css_version+' &'
                else:
                   script= 'nohup /opt/scripts/'+scripts[counter]+' &'

                logging.info("Executing scipt is %s" %(script))
                    
                stdin, stdout, stderr=ssh.exec_command(script,timeout=400) 
                
                logging.info("Command is executed")
                    
                time.sleep(30)
                    
                ssh.close()
            else:
                logging.info("SSH is not required")
                
                    
            logging.info("counter %s is finished" %(counter))
            
        else:
            logging.info("Instance id - %s is in %s state. SSH not possible to start up the applications" %(instance.id,instance.status))
            
        logging.info("Next counter ")
            
        counter = counter + 1
            
except requests.RequestException as e:
        # Send some context about this error to Lambda Logs
    logging.info("Exception occurred ",e.message)