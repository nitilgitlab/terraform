#! /bin/bash

css_ear_version=$1

LOGFILE=pythonsetup_`date +%H:%M:%S`.log

echo "`date +%H:%M:%S` : Basic setup" >> $LOGFILE

echo "`date +%H:%M:%S` : css_ear_version $css_ear_version" >> $LOGFILE

sudo yum install -y https://centos7.iuscommunity.org/ius-release.rpm

sudo yum update -y

sudo yum install wget -y

sudo yum install unzip -y

sudo yum install -y python36u python36u-libs python36u-devel python36u-pip

echo "`date +%H:%M:%S` : Install Python" >> $LOGFILE

python3.6 -V

sudo yum install git -y

cd /tmp

sudo curl "https://s3.amazonaws.com/aws-cli/awscli-bundle.zip" -o "awscli-bundle.zip"

unzip awscli-bundle.zip

sudo ./awscli-bundle/install -i /usr/local/aws -b /usr/local/bin/aws


echo "`date +%H:%M:%S` : Setting up AWS credentials" >> $LOGFILE

mkdir ~/.aws

touch ~/.aws/credentials

echo "[default]
aws_access_key_id=AKIAXUXQOC4ILWV6NQ74
aws_secret_access_key=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" > ~/.aws/credentials

wget https://chelmerterraformdemo.s3-ap-southeast-1.amazonaws.com/StartDemo4.zip --no-check-certificate --no-proxy


echo "`date +%H:%M:%S` : Start Application" >> $LOGFILE

if [ -f /tmp/StartDemo4.zip ] ; then
    echo "StartDemo4 zip found" >> $LOGFILE
    unzip StartDemo4.zip
    echo "Unzip StartDemo4" >> $LOGFILE
    cp /tmp/startapplications.py /tmp/StartDemo4/.
    echo "python3.6 StartDemo4/startapplications.py  $css_ear_version &" >> $LOGFILE
    python3.6 StartDemo4/startapplications.py $css_ear_version
    echo "python3.6 StartDemo4/startapplications.py  finished" >> $LOGFILE
fi

