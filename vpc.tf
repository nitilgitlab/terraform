# Define our VPC
resource "aws_vpc" "demo" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true

  tags={
    Name = "${var.vpc_name}"
  }
}

resource "aws_eip" "nat" {
  vpc = true
}

# Define the public subnet
resource "aws_subnet" "public-subnet" {
  vpc_id = "${aws_vpc.demo.id}"
  cidr_block = "${var.public_subnet_cidr}"
  

  tags={
    Name = "${var.env_name}-PublicSubnet"
  }
}

# Define the private subnet
resource "aws_subnet" "private-subnet-1" {
  vpc_id = "${aws_vpc.demo.id}"
  cidr_block = "${var.private_subnet_1_cidr}"
 

  tags={
    Name = "${var.env_name}-EnvironmentPrivateSubnet1"
  }
}

# Define the private subnet
#resource "aws_subnet" "private-subnet-2" {
#  vpc_id = "${aws_vpc.demo.id}"
#  cidr_block = "${var.private_subnet_2_cidr}"
 

#  tags={
#    Name = "${var.env_name}-EnvironmentPrivateSubnet2"
#  }
#}



# Define the internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.demo.id}"

  tags={
    Name = "${var.env_name}-VPC_IGW"
  }
}


# Define the route table
resource "aws_route_table" "demo-public-rt" {
  vpc_id = "${aws_vpc.demo.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags ={
    Name = "${var.env_name}-PublicSubnetRouteTable"
  }
}

# Define the route table
resource "aws_route_table" "demo-private-rt" {
  vpc_id = "${aws_vpc.demo.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_nat_gateway.gw.id}"
  }

  tags ={
    Name = "${var.env_name}-PrivateSubnetRouteTable"
  }
}

# Assign the route table to the public Subnet
resource "aws_route_table_association" "demo-private-rt1" {
  subnet_id = "${aws_subnet.private-subnet-1.id}"
  route_table_id = "${aws_route_table.demo-private-rt.id}"
}

# Assign the route table to the public Subnet
resource "aws_route_table_association" "demo-private-rt2" {
  subnet_id = "${aws_subnet.private-subnet-2.id}"
  route_table_id = "${aws_route_table.demo-private-rt.id}"
}


# Assign the route table to the public Subnet
resource "aws_route_table_association" "demo-public-rt" {
  subnet_id = "${aws_subnet.public-subnet.id}"
  route_table_id = "${aws_route_table.demo-public-rt.id}"
}


# Define the security group for public subnet
resource "aws_security_group" "sgpublic" {
  name = "${var.env_name}-vpc_hap_sg"
  description = "Allow incoming HTTP connections from cloudflare, internal & SSH access from CIDR"


egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 8443
    to_port = 8443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }

  vpc_id="${aws_vpc.demo.id}"

  tags ={
    Name = "HAProxy Server SG"
  }
}

# Define the security group for public subnet
resource "aws_security_group" "css_sgpublic" {
  name = "${var.env_name}-css_sgpublic"
  description = "Allow incoming HTTP connections from cloudflare, internal & SSH access from CIDR"


egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }


    ingress {
    from_port = 38080
    to_port = 38080
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    from_port = 445
    to_port = 445
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    from_port = 1433
    to_port = 1433
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 8443
    to_port = 8443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }

  vpc_id="${aws_vpc.demo.id}"

  tags ={
    Name = "HAProxy Server SG"
  }
}





#Define the security group for private subnet
resource "aws_security_group" "sql_server"{
  name = "${var.env_name}-internal_sqlserver_sg"
  description = "Allow traffic from private subnet"

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 1433
    to_port = 1433
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }
}

# Define the security group for private subnet
resource "aws_security_group" "sgprivate"{
  name = "${var.env_name}-internal_demo_sg"
  description = "Allow traffic from public subnet"

  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  ingress {
    from_port = 1433
    to_port = 1433
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }
  
  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }


  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    from_port = 38080
    to_port = 38080
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  ingress {
    from_port = 445
    to_port = 445
    protocol = "tcp"
    cidr_blocks = ["${var.vpc_cidr}"]
  }

  vpc_id = "${aws_vpc.demo.id}"

  tags ={
    Name = "Internal SG"
  }
}

# Define the aws_nat_gateway for private subnet instances and attached to public subnet
resource "aws_nat_gateway" "gw" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.public-subnet.id}"

  tags = {
    Name = "${var.env_name} gw NAT"
  }
}