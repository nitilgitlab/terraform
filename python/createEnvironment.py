import json

import requests
import boto3
import paramiko
import botocore
import time
import sys
import logging
import os



logging.basicConfig(filename='createEnvironment.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s',level=logging.DEBUG)

logging.info('This is an info message')

print ("the script has the environment name %s" % (sys.argv[0]))

env_name = sys.argv[1]

vpc_name = env_name + "_vpc"

logging.info("vpc_name  %s" % (vpc_name))


region_name = sys.argv[2]

logging.info("region_name  %s" % (region_name))

css_ear_version = sys.argv[3]

logging.info("css_ear_version  %s" % (css_ear_version))


#if(css_ear_version.find("Suite")):
 # logging.info("css_ear_version  %s contains Suite so it requires extraction" % (css_ear_version))
 # version=css_ear_version.split('-')[0]
 # logging.info("version is %s" %(version))
 # css_ear_version=version+"-SNAPSHOT"
 # logging.info("css_ear_version is %s" %(css_ear_version)) 


logging.info ("request received for creating new environment %s" %(env_name))

try:
    
    ip = requests.get("http://checkip.amazonaws.com/")
    ec2 = boto3.resource('ec2',region_name)
    
    client = boto3.client('ec2',region_name)

    filters = [{'Name':'tag:Name', 'Values':[env_name]}]

    vpcs = list(ec2.vpcs.filter(Filters=filters))

    for vpc in vpcs:
        response = client.describe_vpcs(
            VpcIds=[
                vpc.id,
            ]
    )
        print(json.dumps(response, sort_keys=True, indent=4))
    
    logging.info ("vpcs size %s" % (len(vpcs)))

    if len(vpcs) == 0:
        
        cmd= 'rm -r ../terraform_environments/'+env_name
        logging.info('destroying if environment folder exists %s' %(env_name))
        logging.info(cmd)
        #cmd=cmd,env_name
        os.system(cmd)
        
        cmd= 'mkdir -p ../terraform_environments/'+env_name
        logging.info('creating new environment %s' %(env_name))
        logging.info(cmd)
        #cmd=cmd,env_name
        os.system(cmd)
        copy_command= 'cp -rf ../terraform/* ../terraform_environments/'+env_name+'/.'
        logging.info('copy_command is %s' %(copy_command))
        os.system(copy_command)
    
        env_provision_cmd = 'terraform init && terraform apply --auto-approve -var "env_name='+env_name+'" -var "vpc_name= '+vpc_name+'" -var "css_ear_version='+css_ear_version+'" '  #-var "aws_region= '+region_name+'" 
        
        env_provision_cmd= 'cd ../terraform_environments/'+env_name+' && '+env_provision_cmd
        logging.info(env_provision_cmd)
        os.system(env_provision_cmd)       
        
        #cmd = 'cd .. && terraform apply --auto-approve -var '
        #cmd = cmd + '"env_name="'+env_name
        #logging.info (env_provision_cmd, env_name,vpc_name,region_name)
        logging.info (env_provision_cmd)
        #os.system('ls -ltr && '+env_provision_cmd)
        logging.info ("environment doesn't exists. Create new environment folder for terraform")
    else:
        
        logging.info ("environment already exists. so no need to create new folder")
except requests.RequestException as e:
        # Send some context about this error to Lambda Logs
    logging.error("Exception occurred %s" %(e.message))