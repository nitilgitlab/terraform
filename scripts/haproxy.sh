#!/bin/bash

alfr=$1
auth=$2
css=$3

echo "alfr is $alfr"
sed -i '' 's|web-demo-alfr.web-apps.co.nz|'"$alfr"'|g' haproxy.cfg
   timeout queue           1m
    timeout connect         61s
        timeout tarpit                  5m
    timeout client          1m
    timeout server          1m
    timeout http-keep-alive 61s
    timeout check           61s
    maxconn                 10000

#---------------------------------------------------------------------
# main frontend which proxys to the backends
#---------------------------------------------------------------------

frontend authenticate.webapps-front
    bind 192.168.0.116:8443 ssl crt /etc/haproxy/web-apps.co.nz.pem 
    bind 192.168.0.116:8080
    mode http
#    acl chelmer-net src 10.0.0.0/16

#    http-request allow if chelmer-net
#    http-request tarpit if { method POST } { path_beg /secure/json/authenticate } { url /secure/json/authenticate?locale=en-US }
    default_backend authenticate.webapps-back

frontend demo-alfr-front
    bind 192.168.1.71:80
    bind 192.168.1.71:443 ssl crt /etc/haproxy/web-apps.co.nz.pem