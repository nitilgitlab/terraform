#!/bin/bash

LOGFILE=startcss_`date +%H:%M:%S`.log


echo "`date +%H:%M:%S` : Starting work" >> $LOGFILE

service iptables stop >> $LOGFILE

echo "`date +%H:%M:%S` : iptables stopped" >> $LOGFILE


ps -ef | grep jboss | grep -v grep | awk '{print $2}' | xargs kill


if [ -f /var/lock/subsys/wildfly ] ; then
 echo "/var/lock/subsys/wildfly exists"
 chown -R jboss /var/lock/subsys/wildfly
fi

chown -R jboss /app/jboss

#chown -R jboss:jboss /app/jboss/wildfly

su - jboss

echo "`date +%H:%M:%S` : switched to user jboss" >> $LOGFILE

echo "`date +%H:%M:%S` : going to start css server" >> $LOGFILE

/etc/init.d/css start >> $LOGFILE 2>1 &

echo "`date +%H:%M:%S` : sleeping after css star command" >> $LOGFILE

sleep 240s

ps -ef | grep jboss | grep -v grep | awk '{print $2}' >> $LOGFILE

/etc/init.d/css status >> $LOGFILE

echo "`date +%H:%M:%S` : going to start jbpm server" >> $LOGFILE

/etc/init.d/jbpm start >> $LOGFILE 2>1 &&

echo "`date +%H:%M:%S` : sleeping after jbpm star command" >> $LOGFILE
sleep 180s


echo "`date +%H:%M:%S` : script finished" >> $LOGFILE