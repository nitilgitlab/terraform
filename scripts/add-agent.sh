#!/bin/bash

USAGE="USAGE: sudo ./$0 [properties file]"

if [[ $# -ne 1 ]]; then
        echo
        echo $USAGE
        echo
        exit -1;
fi

declare -A array
while IFS="=" read f1 f2 
do 
        if [ -z $f1 ]; then     # ignore empty lines
                continue;
        fi
        array[$f1]=$f2;
done < work/openam.properties

# Store the property file as an associative array for later use
while IFS="=" read f1 f2 
do 
        if [ -z $f1 ]; then     # ignore empty lines
                continue;
        fi
        array[$f1]=$f2;
done < $1


# Replace TOKENS i.e. @TOKEN@ in token file
function replaceTokens {
        for i in "${!array[@]}"; do
                replace=${array[$i]};
                sed -i "s|\@$i\@|$replace|g" $1
        done
}

function storeAttribute {
        cp $1 temp.txt
        sed -i -e 's/":"/=/g' temp.txt
        sed -i -e 's/","/\n/g' temp.txt
        sed -i -e 's/"}/\n/g' temp.txt
        sed -i -e 's/{"/\n/g' temp.txt

        while IFS="=" read f1 f2 
        do 
                if [[ $f1 = "$2" ]]; then
                        array[$2]=$f2 
                fi
        done < temp.txt
        rm temp.txt
}
runas_user=${array['RUNAS_USER']};
runas_group=${array['RUNAS_GROUP']}

if [[ -z $runas_user  ||  -z $runas_group ]]; then
        echo 'You must specify a user and group to install this module...'
        exit 1
fi

echo Checking if user [ $runas_user ] is a member of group [ $runas_group ]...

USR=$(id $runas_user)
if [ $? != "0" ]; then
        echo User [ $runas_user ] does not exist on this system...
    echo 'You must specify a valid user and group to install this module...'
        exit 1
fi

if [[ ! $USR =~  .*groups=.*\($runas_group\).* ]];
then
        echo 'User [' $runas_user '] is not a member of the Group [' $runas_group '] ...'
        echo 'You must specify a valid user and group to install this module...'
        exit 1
fi
echo Success....

install_dir=${array['INSTALL_DIR']}
install_root=$install_dir/openam

# Check the zone realm
zone=${array['ZONE']}
if [ -z $zone ] || [[ $zone = */* ]]; then 
        echo Zone is not valid...
        exit 1
fi

# read zone properties to get auth chain
while IFS="=" read f1 f2 
do 
        if [ -z $f1 ]; then     # ignore empty lines
                continue;
        fi
        array[$f1]=$f2;
done < work/${array['ZONE'],,}.properties 

IFS="," read -r -a domain <<< ${array['DOMAIN_SUFFIX']}
domain_id=$(echo ${domain,,} | cut -d'=' -f2)
        
realm=${array['REALM']}
if [[ $realm = */* ]]; then 
        echo Realm $realm is not valid...
        exit 1
fi

# create and store realm paths
if [ -z "$realm" ]; then 
        realm_path="$zone"
        realm_ou="o=$zone"
else
        realm_path="$zone/$realm"
        realm_ou="o=$realm,o=$zone"
fi
array['REALM_PATH']=$realm_path
array['REALM_OU']=$realm_ou


if [ ${array['APP_TYPE']} = "CSS" ]; then

        echo Creating realm $realm_path...
        $install_root/ssoAdminTools/${array['OPENAM_URI']}/bin/ssoadm create-realm -e /$realm_path -u amadmin -f .pwd
        if [ $? != "0" ]; then
                echo Failed to create realm...
                exit 1
        fi

        # Check and create the JDBC authentication module
        jdbc_module_name=FusionDB
        cp templates/JDBC-auth-module.properties.template work/JDBC-auth-module.properties
        replaceTokens work/JDBC-auth-module.properties

        echo Creating JDBC module $jdbc_module_name...
        $install_root/ssoAdminTools/${array['OPENAM_URI']}/bin/ssoadm create-auth-instance -u amadmin -f .pwd -e /$realm_path -t JDBC -m $jdbc_module_name
        $install_root/ssoAdminTools/${array['OPENAM_URI']}/bin/ssoadm update-auth-instance -u amadmin -f .pwd -e /$realm_path -m $jdbc_module_name -D work/JDBC-auth-module.properties
        if [ $? != "0" ]; then
                echo Failed to update JDBC authentication module...
        fi

        echo Adding $jdbc_module_name to $domain_id authentication chain...
        $install_root/ssoAdminTools/${array['OPENAM_URI']}/bin/ssoadm add-auth-cfg-entr -u amadmin -f .pwd -e /$realm_path -c REQUIRED -o $jdbc_module_name -m $domain_id -t iplanet-am-auth-shared-state-behavior-pattern=tryFirstPass
        if [ $? != "0" ]; then
                echo Failed to add $jdbc_module_name to authentication chain...
        fi
        
        if [ ${array['CONFIGURE_AD'],,} = "true" ]; then
                if [ ${array['CONFIGURE_WDSSO'],,} = "true" ]; then
                        echo Adding $jdbc_module_name to ${domain_id}SSO authentication chain...
                        $install_root/ssoAdminTools/${array['OPENAM_URI']}/bin/ssoadm add-auth-cfg-entr -u amadmin -f .pwd -e /$realm_path -c SUFFICIENT -o $jdbc_module_name -m ${domain_id}SSO -t iplanet-am-auth-shared-state-behavior-pattern=tryFirstPass
                        if [ $? != "0" ]; then
                                echo Failed to add $jdbc_module_name to ${domain_id}SSO authentication chain...
                        fi
                fi
  fi
        
        cp templates/JDBC-datastore.properties.template work/JDBC-datastore.properties
        replaceTokens work/JDBC-datastore.properties
                
        echo Creating FusionDB datastore...
        $install_root/ssoAdminTools/${array['OPENAM_URI']}/bin/ssoadm create-datastore -u amadmin -f .pwd -e /$realm_path -t database -m FusionDB -D work/JDBC-datastore.properties
        if [ $? != "0" ]; then
          echo Failed to create FusionDB datastore...
        fi
        
        echo Installing realm referral policy...
        cp templates/realm-referral-policy.xml.template work/realm-referral-policy.xml
        replaceTokens work/realm-referral-policy.xml

        $install_root/ssoAdminTools/${array['OPENAM_URI']}/bin/ssoadm create-policies -u amadmin -f .pwd -e /$zone -X work/realm-referral-policy.xml
        if [ $? != "0" ]; then
                echo Failed to install the realm referral policy...
        fi
        echo Success...
        
        cp templates/css-policies.xml.template work/policies.xml

elif  [ ${array['APP_TYPE']} = "Alfresco" ] || [ ${array['APP_TYPE']} = "Alfresco2" ]; then
        cp templates/alfresco-policies.xml.template work/policies.xml
else
        echo "The APP_TYPE property is set to an undefined value"
fi 

replaceTokens work/policies.xml
$install_root/ssoAdminTools/${array['OPENAM_URI']}/bin/ssoadm create-policies -u amadmin -f .pwd -e /$realm_path  -X work/policies.xml
if [ $? != "0" ]; then
        echo Failed to install the policies...
fi
echo Success...

echo Adding J2EE agent profile...
if [ ${array['APP_TYPE']} = "CSS" ]; then
        cp templates/css-agent.properties.template work/agent.properties
else
        cp templates/alfresco-agent.properties.template work/agent.properties
fi
replaceTokens work/agent.properties

PROFILE_NAME=${array['PROFILE_NAME']}

echo Creating agent profile $PROFILE_NAME... 
$install_root/ssoAdminTools/${array['OPENAM_URI']}/bin/ssoadm create-agent -v -u amadmin -f .pwd -e /$realm_path -b $PROFILE_NAME -t J2EEAgent -a userpassword="${array['AGENT_PASSWORD']}" -s "http://${array['OPENAM_HOST']}:${array['OPENAM_PORT']}/${array['OPENAM_URI']}" -g "http://${array['APP_HOST']}:8080/agentapp" -D work/agent.properties
if [ $? != "0" ]; then
         echo Failed to create profile...
fi
        
# create the password file for the agent bootstrap file
echo "${array['AGENT_PASSWORD']}" > work/agent.pw

echo Creating agent OpenSSOAgentBootstrap.properties file...
echo "if the next step hangs run ./agent/j2ee_agents/jboss_v42_agent/bin/agentadmin manually and accept eula before retrying"

if [ ${array['APP_TYPE']} = "CSS" ]; then
        # Now get the encrypted password
        secret=`echo $(./agent/j2ee_agents/jboss_v42_agent/bin/agentadmin --encrypt Agent_001 work/agent.pw < eula.txt) | cut -d':' -f2 `
        array['SECRET']="$secret"
        
        array['AGENT_FOLDER']="jboss_v42_agent"
        array['AGENT_SERVICE_RESOLVER']="jboss.v40.AmJBossAgentServiceResolver"
        array['AGENT_ENCRYPTION_PASSWORD']="L/nClg2n7NA5RrjUFNz0FBsyHK2JKV0"
        
        cp templates/OpenSSOAgentBootstrap.properties.template $install_root/setup/agent/css/openam/j2ee_agents/jboss_v42_agent/Agent_001/config/OpenSSOAgentBootstrap.properties
        replaceTokens $install_root/setup/agent/css/openam/j2ee_agents/jboss_v42_agent/Agent_001/config/OpenSSOAgentBootstrap.properties
        cp templates/OpenSSOAgentConfiguration.properties.template $install_root/setup/agent/css/openam/j2ee_agents/jboss_v42_agent/Agent_001/config/OpenSSOAgentConfiguration.properties
        replaceTokens $install_root/setup/agent/css/openam/j2ee_agents/jboss_v42_agent/Agent_001/config/OpenSSOAgentConfiguration.properties
        
        echo pushing OpenAM agent configuration to CSS client...
        echo scp -pr $install_root/setup/agent/css/* jboss@${array['APP_HOST']}:/app
        sshpass -p jboss scp -pr $install_root/setup/agent/css/* jboss@${array['APP_HOST']}:/app
        echo Success...

else
        # Now get the encrypted password
        secret=`echo $(./agent/j2ee_agents/tomcat_v6_agent/bin/agentadmin --encrypt Agent_001 work/agent.pw < eula.txt) | cut -d':' -f2 `
        array['SECRET']="$secret"
        
        array['AGENT_FOLDER']="tomcat_v6_agent"
        array['AGENT_SERVICE_RESOLVER']="tomcat.v6.AmTomcatAgentServiceResolver"
        array['AGENT_ENCRYPTION_PASSWORD']="mb8fB38NqgvMujp9if6IfAs4A/rkXiFQ"
        
        cp templates/OpenSSOAgentBootstrap.properties.template $install_root/setup/agent/alfresco/openam/j2ee_agents/tomcat_v6_agent/Agent_001/config/OpenSSOAgentBootstrap.properties
        replaceTokens $install_root/setup/agent/alfresco/openam/j2ee_agents/tomcat_v6_agent/Agent_001/config/OpenSSOAgentBootstrap.properties
        cp templates/OpenSSOAgentConfiguration.properties.template $install_root/setup/agent/alfresco/openam/j2ee_agents/tomcat_v6_agent/Agent_001/config/OpenSSOAgentConfiguration.properties
        replaceTokens $install_root/setup/agent/alfresco/openam/j2ee_agents/tomcat_v6_agent/Agent_001/config/OpenSSOAgentConfiguration.properties
        echo Success...
        
        echo pushing OpenAM agent configuration to Alfresco client...
        if [ ${array['APP_TYPE']} = "Alfresco" ]; then
                echo scp -pr $install_root/setup/agent/alfresco/* jboss@${array['APP_HOST']}:/app
                echo scp -pr $install_root/setup/agent/tomcat jboss@${array['APP_HOST']}:/app/alfresco
                scp -pr $install_root/setup/agent/alfresco/* jboss@${array['APP_HOST']}:/app
                scp -pr $install_root/setup/agent/tomcat jboss@${array['APP_HOST']}:/app/alfresco
        else
                echo scp -pr $install_root/setup/agent/alfresco/openam/j2ee_agents/tomcat_v6_agent/Agent_001/config/* root@${array['APP_HOST']}:/app/alfresco/tomcat/lib
                echo scp -pr $install_root/setup/agent/tomcat root@${array['APP_HOST']}:/app/alfresco
                scp -pr $install_root/setup/agent/alfresco/openam/j2ee_agents/tomcat_v6_agent/Agent_001/config/* root@${array['APP_HOST']}:/app/alfresco/tomcat/lib
                scp -pr $install_root/setup/agent/tomcat root@${array['APP_HOST']}:/app/alfresco
        fi
        
        echo Success...

fi

echo OpenAM configured for $realm_path
(END)