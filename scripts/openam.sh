#!/bin/bash

yum install sshpass -y

cd /app/openam/setup

./add-agent.sh web-demo2-css.properties

service openam stop

sleep 10

service openam start

sleep 15