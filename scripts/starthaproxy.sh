#!/bin/bash

LOGFILE=startcss_`date +%H:%M:%S`.log

echo "`date +%H:%M:%S` : HAPROXY Starting work" >> $LOGFILE

service haproxy restart

echo "`date +%H:%M:%S` : HAPROXY script finished" >> $LOGFILE