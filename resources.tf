# Define SSH key pair for our instances
resource "aws_key_pair" "default" {
  key_name = "${var.env_name}-vpcdemokeypair"
  public_key = "${file("id_rsa_terraform_lab.pub")}"
}


resource "aws_route53_zone" "web-apps" {
name = "web-apps.co.nz"

vpc {
vpc_id = "${aws_vpc.demo.id}"
}
}

resource "aws_route53_zone" "chelmer" {
name = "chelmer.co.nz"

vpc {
vpc_id = "${aws_vpc.demo.id}"
}
}

# Define openam server inside the private subnet
resource "aws_instance" "openam" {
   ami  = "${var.openam-ami}"
   instance_type = "t2.micro"
   key_name = "${aws_key_pair.default.key_name}"
   subnet_id = "${aws_subnet.private-subnet-1.id}"
   vpc_security_group_ids = ["${aws_security_group.sgprivate.id}"]
   #associate_public_ip_address = true
   source_dest_check = false
   #user_data = "${file("install.sh")}"

    root_block_device {
    delete_on_termination = true
    }

depends_on = [aws_nat_gateway.gw]
  tags= {
    Name = "${var.env_name}-openam_auth_server "
  }

}

resource "aws_route53_record" "openam" {
zone_id = "${aws_route53_zone.web-apps.zone_id}"
name = "core-dmz-oam1.web-apps.co.nz"
type = "A"
ttl = "300"
records = ["${aws_instance.openam.private_ip}"]
}


# Define openam server inside the private subnet
resource "aws_instance" "alfresco" {
   ami  = "${var.alfresco-ami}"
   instance_type = "t2.micro"
   key_name = "${aws_key_pair.default.key_name}"
   #key_name = "${file("id_rsa_terraform_lab.pub")}"
   subnet_id = "${aws_subnet.private-subnet-1.id}"
   vpc_security_group_ids = ["${aws_security_group.sgprivate.id}"]
   #associate_public_ip_address = true
   source_dest_check = false
   #user_data = "${file("install.sh")}"

    root_block_device {
    delete_on_termination = true
 }

   user_data = <<EOF
#!/bin/bash
sudo hostnamectl set-hostname web-demo-alfr.web-apps.co.nz
echo '${aws_instance.openam.private_ip} alfr-aws.web-apps.co.nz' | sudo tee -a /etc/hosts
EOF

  tags= {
    Name = "${var.env_name}-alfresco_server"
  }
  depends_on = [aws_nat_gateway.gw]
}


resource "aws_route53_record" "alfresco" {
zone_id = "${aws_route53_zone.web-apps.zone_id}"
name = "web-demo-alfr.web-apps.co.nz"
type = "A"
ttl = "300"
records = ["${aws_instance.alfresco.private_ip}"]
}


# Define openam server inside the private subnet
resource "aws_instance" "css" {
   ami  = "${var.css-ami}"
   instance_type = "m4.xlarge"
   key_name = "${aws_key_pair.default.key_name}"
   #subnet_id = "${aws_subnet.private-subnet-1.id}"
   subnet_id = "${aws_subnet.public-subnet.id}"
   #vpc_security_group_ids = ["${aws_security_group.sgprivate.id}"]
   vpc_security_group_ids = ["${aws_security_group.css_sgpublic.id}"]
   associate_public_ip_address = true
   source_dest_check = false
   #user_data = "${file("install.sh")}"

    root_block_device {
    delete_on_termination = true
 }

   # Copies the haproxy.cfg file to /tmp/haproxy.cfg
  provisioner "file" {

  connection {
            host = "${self.public_ip}"
            type     = "ssh"
            user     = "root"
            password = "${var.ssh_password}"
        }

    source      = "install_css_ear.sh"
    destination = "/opt/scripts/install_css_ear.sh"
  }

  provisioner "file" {

  connection {
            host = "${self.public_ip}"
            type     = "ssh"
            user     = "root"
            password = "${var.ssh_password}"
        }

    source      = "/home/centos/css-ear-install"
    destination = "/home/bamboo"
  }

  tags= {
    Name = "${var.env_name}-css_server"
  }
  depends_on = [aws_nat_gateway.gw]
}

resource "aws_route53_record" "css" {
zone_id = "${aws_route53_zone.web-apps.zone_id}"
name = "web-demo2-css.web-apps.co.nz"
type = "A"
ttl = "300"
records = ["${aws_instance.css.private_ip}"]
}


# Define openam server inside the private subnet
resource "aws_instance" "sqlserver" {
   ami  = "${var.sqlserver-ami}"
   instance_type = "m4.xlarge"
   key_name = "${aws_key_pair.default.key_name}"
   subnet_id = "${aws_subnet.private-subnet-1.id}"
   vpc_security_group_ids = ["${aws_security_group.sgprivate.id}"]
   #associate_public_ip_address = true
   
   source_dest_check = false
   #user_data = "${file("install.sh")}"

    root_block_device {
    delete_on_termination = true
 }

  tags= {
    Name = "${var.env_name}-sql_server"
  }
  depends_on = [aws_nat_gateway.gw]
}


resource "aws_route53_record" "sqlserver" {
zone_id = "${aws_route53_zone.chelmer.zone_id}"
name = "chm-qa-db01.chelmer.co.nz"
type = "A"
ttl = "300"
records = ["${aws_instance.sqlserver.private_ip}"]
}


# Define openam server inside the private subnet
resource "aws_instance" "haproxy" {
   ami  = "${var.haproxy-ami}"
   instance_type = "m4.micro"
   key_name = "${aws_key_pair.default.key_name}"
   subnet_id = "${aws_subnet.public-subnet.id}"
   vpc_security_group_ids = ["${aws_security_group.sgpublic.id}"]
   associate_public_ip_address = true
   source_dest_check = false
   root_block_device {
    delete_on_termination = true
 }

  #provisioner "local-exec" {
   # command = "./haproxy.sh ${aws_route53_record.openam.name} sf saf "
  #}


   # Copies the haproxy.cfg file to /tmp/haproxy.cfg
  provisioner "file" {

  connection {
            host = "${self.public_ip}"
            type     = "ssh"
            user     = "root"
            password = "${var.ssh_password}"
        }

    source      = "haproxy.cfg"
    destination = "/etc/haproxy/haproxy.cfg"
  }


  

  provisioner "file" {

  connection {
            host = "${self.public_ip}"
            type     = "ssh"
            user     = "root"
            password = "${var.ssh_password}"
        }
    

    source      = "id_rsa_terraform_lab.pub"
    destination = "~/.ssh/authorized_keys"
  }


  provisioner "file" {

  connection {
            host = "${self.public_ip}"
            type     = "ssh"
            user     = "root"
            password = "Rev0lt1x01"
        }
    

    source      = "id_rsa_terraform_lab"
    destination = "/tmp/id_rsa_terraform_lab"
  }

    provisioner "remote-exec" {
    connection {
            host = "${self.public_ip}"
            type     = "ssh"
            user     = "root"
            password = "Rev0lt1x01"
        }

    inline = [
      "service haproxy restart"
    ]
  }

   


  #user_data = "${file("install.sh")}"


  # user_data = <<EOF
  # EOF

  tags= {
    Name = "${var.env_name}-haproxy_server"
  }

   depends_on = [aws_instance.openam,aws_instance.css,aws_instance.alfresco]
}

resource "template_file" "etchosts" {
  template = "${file("templates/csshosts.tpl")}"
  vars ={
    alfr-aws_public_url = "${var.alfr-aws_public_url}"
    auth-aws_public_url = "${var.auth-aws_public_url}"
    demo-css_public_url = "${var.demo-css_public_url}"
    alfresco_public_ip  = "${aws_instance.haproxy.public_ip}"
    auth_public_ip      = "${aws_instance.haproxy.public_ip}"
    css_public_ip       = "${aws_instance.haproxy.public_ip}"
  }

   depends_on = [aws_instance.openam,aws_instance.css,aws_instance.alfresco]
}

resource "template_file" "copyscript" {
  template = "${file("templates/copyscripts.tpl")}"
  vars ={
    alfresco_private_ip = "${aws_instance.alfresco.private_ip}"
    css_private_ip = "${aws_instance.css.private_ip}"
    haproxy_private_ip = "${aws_instance.haproxy.private_ip}"
    openam_private_ip = "${aws_instance.openam.private_ip}"
    ssh_pass="${var.ssh_password}"
    
  }

   depends_on = [aws_instance.openam,aws_instance.css,aws_instance.alfresco]
}

resource "template_file" "webprop" {
  template = "${file("templates/web.properties.tpl")}"
  vars ={
    env_name = "${var.env_name}"
  }
}

resource "template_file" "webdemocssprop" {
  template = "${file("templates/web-demo2-css.properties.tpl")}"
  vars ={
    env_name = "${var.env_name}"
  }
}

resource "template_file" "startapplicationscript" {
  template = "${file("templates/startapplications.py.tpl")}"
  vars ={
    css_instance_id = "${aws_instance.css.id}"
    haproxy_instance_id = "${aws_instance.haproxy.id}"
    alfr_instance_id = "${aws_instance.alfresco.id}"
    openam_instance_id = "${aws_instance.openam.id}"
    aws_region="${var.aws_region}"
  }

   depends_on = [aws_instance.openam,aws_instance.css,aws_instance.alfresco]
}



# Define openam server inside the private subnet
resource "aws_instance" "bastionhost" {
   ami  = "${var.bastionhost-ami}"
   instance_type = "t2.micro"
   key_name = "${aws_key_pair.default.key_name}"
   subnet_id = "${aws_subnet.public-subnet.id}"
   vpc_security_group_ids = ["${aws_security_group.sgpublic.id}"]
   associate_public_ip_address = true
   source_dest_check = false
   #user_data = "${file("install.sh")}"

   provisioner "file" {

    connection {
      host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }

    content      = "${template_file.etchosts.rendered}"
    destination = "/tmp/config.json"
  }

  provisioner "file" {
     connection {
       host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      = "${template_file.copyscript.rendered}"
    destination = "/tmp/copyscript.sh"
  }

    provisioner "file" {
     connection {
       host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      =  "${file("add-agent.sh")}"
    destination = "/tmp/add-agent.sh"
  }

   provisioner "file" {
     connection {
       host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      =  "${file("openam.sh")}"
    destination = "/tmp/openam.sh"
  }

  provisioner "file" {
     connection {
       host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      =  "${file("realmsetup.sh")}"
    destination = "/tmp/realmsetup.sh"
  }

  provisioner "file" {
     connection {
       host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      = "${template_file.webprop.rendered}"
    destination = "/tmp/web.properties"
  }

    provisioner "file" {
     connection {
       host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      = "${template_file.webdemocssprop.rendered}"
    destination = "/tmp/web-demo2-css.properties"
  }

provisioner "file" {
   connection {
     host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      = "${file("startcss.sh")}"
    destination = "/tmp/startcss.sh"
  }

  provisioner "file" {
   connection {
     host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      = "${file("starthaproxy.sh")}"
    destination = "/tmp/starthaproxy.sh"
  }

  provisioner "file" {
   connection {
     host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      = "${file("pythonsetup.sh")}"
    destination = "/tmp/pythonsetup.sh"
  }

    provisioner "file" {
   connection {
     host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      = "${template_file.startapplicationscript.rendered}"
    destination = "/tmp/startapplications.py"
  }


provisioner "file" {
   connection {
     host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    content      = "${file("ssh_config")}"
    destination = "~/.ssh/config"
  }

  provisioner "remote-exec" {
     connection {
       host = "${self.public_ip}"
    type = "ssh"
    user = "centos"
    private_key = "${file("id_rsa_terraform_lab")}"
  }
    inline = [
      "chmod +x /tmp/copyscript.sh",
      "chmod +x /tmp/openam.sh",
      "chmod +x /tmp/realmsetup.sh",
      ". /tmp/copyscript.sh",
      "chmod +x /tmp/pythonsetup.sh",
      ". /tmp/pythonsetup.sh ${var.css_ear_version}"
    ]
  }


  tags= {
    Name = "${var.env_name}-bastionhost"
  }

   depends_on = [aws_instance.openam,aws_instance.haproxy,aws_instance.css,aws_instance.alfresco,template_file.etchosts,template_file.copyscript]
}

# Define openam server inside the private subnet
#resource "aws_instance" "windowsbastionhost" {
#   ami  = "${var.windowsbastion-ami}"
#   instance_type = "t2.medium"
#   key_name = "${aws_key_pair.default.key_name}"
#   subnet_id = "${aws_subnet.public-subnet.id}"
#   vpc_security_group_ids = ["${aws_security_group.sgpublic.id}"]
#   associate_public_ip_address = true
#   source_dest_check = false
#   #user_data = "${file("install.sh")}"

   #User name	Administrator
   #Password	3CBIumAYh&*Cqm;dQ.ck!2PaiZhGg%&a


#  tags= {
#    Name = "windows bastion host"
#  }
#}


# Define openam server inside the private subnet
#resource "aws_instance" "bastionhost" {
#   ami  = "${var.bastionhost-ami}"
#   instance_type = "m4.xlarge"
#   key_name = "demo-ec2"
#   subnet_id = "${aws_subnet.public-subnet.id}"
#   vpc_security_group_ids = ["${aws_security_group.sgpublic.id}"]
#   associate_public_ip_address = true
#   source_dest_check = false

#  tags= {
#    Name = "bastion host"
#  }

#     provisioner "remote-exec" {
#        inline = [
#            //Executing command to creating a file on the instance
#            "echo 'Some data' > SomeData.txt",
#        ]

        //Connection to be used by provisioner to perform remote executions
 #       connection {
 #           //Use public IP of the instance to connect to it.
 #           host          = "${aws_instance.bastionhost.public_ip}"
 #           type          = "ssh"
 #           user          = "ec2-user"
 #           private_key   = "${file("demo-ec2.pem")}"
 #           timeout       = "1m"
 #           agent         = false
 #       }
 #   }
#}

resource "null_resource" "alfresco-exec" {
  triggers = {
      build_number = "${timestamp()}"
  }

  #connection {
  #  type = "ssh"
  #  user = "ubuntu"
  #  host = "${aws_instance.alfresco.private_ip}"
  #  private_key = file("id_rsa")
  #}

  provisioner "local-exec" {
    command = "rm private_ips.txt"
   
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.openam.private_ip} auth-aws.web-apps.co.nz >> private_ips.txt"
   
  }

  provisioner "local-exec" {
    command = "echo ${aws_instance.alfresco.private_ip} alfr-aws.web-apps.co.nz >> private_ips.txt"
   
  }



 # provisioner "remote-exec" {
 #   inline = [
 #     "echo test",
 #     "sudo hostnamectl set-hostname auth-aws.web-apps.co.nz",
 #     "echo '127.0.0.1 auth-aws.web-apps.co.nz' | sudo tee /etc/hosts",
 #     "echo '${aws_instance.alfr.private_ip} alfr-aws.web-apps.co.nz' | sudo tee -a /etc/hosts",
 #     "echo '${aws_instance.demo4.private_ip} demo4-aws.web-apps.co.nz' | sudo tee -a /etc/hosts"
 #   ]
 # }

  depends_on = [aws_instance.haproxy,aws_instance.openam,aws_instance.css,aws_instance.alfresco]
}
